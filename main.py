import pandas as pd
import os
import importlib
import random

from src.models.benchmark import benchmark
from src.models.benchmark_charts import get_charts
from src.data_processing.data_processing import read_stock_prices, read_fundamental_data, read_data
from src.models.svm import Svm
from src.tools.visualization import visualize, visualize_test
from src.scraping.FundamentalAnalysis import update_stock_information, check_data

from src.logger import Logger

# TODO: FIXME
# Add an option to use a pca while loading the fundamental data
# Add more parameters for the models
update_stock_information("K")
#check_data("K")
df = read_data("K")
exit(0)
################# SCRAPING PHASE
df = pd.read_csv("NYSE.csv", sep='\t')
symbols = list(filter(lambda x: not '.' in x and not '-' in x, df.Symbol))

for index, sb in enumerate(symbols):

    print("step {}/{}: {}".format(index, len(symbols), sb))
    update_stock_information(sb)

###########################################################


fundamental_data = read_fundamental_data("stockpup.pkl", threshold_correlation=0.0, pca_components=10)

benchmark_file = "benchmark1.txt"
nb_stocks = 3

models_params = {
    "LastValue": {
        "N": [1, 8, 3],
    },
    "AverageWindow": {
        "N": [2, 9, 3],
    },
    "Prophet": {
        "seasonality": [0, 3, 1],
        "changepoint_prior_scale": [0.05, 2.1, 1],
        #"fourier_order": [0, 10, 5],
        "mcmc_samples": [0, 100, 50],
        "uncertainty_samples": [0, 2500, 750],
    },
    "Lstm": {
        "sliding_window": [5, 55, 15],
        "moving_average_window": [0, 10, 3],
    }
}

# Benchmark
if not os.path.isfile(os.path.join("logs", benchmark_file)):
    
    logger = Logger(benchmark_file, verbose=2)
    benchmark(nb_stocks, logger, models_params)
    # Kill the logger to save the file
    logger.kill()

# Display the charts from the benchmark
best_params = get_charts(os.path.join("logs", benchmark_file))

nb_stock = 1
df_name = "looooooooooool"
while not os.path.exists("stock_prices/{}.csv".format(df_name)):
    df_name, df_fund = random.sample(fundamental_data.items(), nb_stock)[0]

print(df_name)

# Set the train and test window variables
test_days = 60
train_days = 800
# Get the dataframe(s) to visualize the data on
stock_names = [df_name]
dfs = [(read_stock_prices(stock, train_days=train_days, test_days=test_days)) for stock in stock_names]

for data in dfs:

    df, df_test = data

    predictions = dict()

    for model_name in models_params.keys():

        # Load "module.submodule.model_name"
        model_object = getattr(importlib.import_module("src.models"), model_name)
        # Instantiate the class (pass arguments to the constructor, if needed)
        model = model_object(**best_params[model_name])
        preds = model.predict(df, future_days=test_days)
        predictions[model_name] = preds


    svm = Svm()
    df_preds = svm.predict(df_fund)
    print(df_preds.head())
    predictions["svm"] = df_preds

    # Visualization
    visualize(df, df_test, predictions)
    visualize_test(df_test, predictions)