import pandas as pd
import numpy as np

from sklearn import svm
from src.models import Model
from sklearn.preprocessing import StandardScaler
from src.tools.tools import handle_format_predictions


class Svm(Model):

    def __init__(self, C: float = 1.0, kernel: str = 'rbf', degree: int = 3):

        super(Svm, self).__init__(C=C, kernel=kernel, degree=degree)
        self.svm = svm.SVR(C=self.C, kernel=self.kernel, degree=self.degree)


    def predict(self, df: pd.DataFrame, future_days=90):

        future_quarters = max(future_days // 90, 1)

        y = df["New Price"].values.reshape(-1, 1)
        X = df.drop(columns=["New Price"]).values

        std_scaler = StandardScaler()
        X_scaled = std_scaler.fit_transform(X)
        y_scaled = std_scaler.fit_transform(y)

        X_train, y_train = X_scaled[:-future_quarters], y_scaled[:-future_quarters]
        X_test, _ = X_scaled[-future_quarters:], y_scaled[-future_quarters:]

        self.svm.fit(X_train, y_train)

        y_preds_scaled = self.svm.predict(X_test)
        y_preds = std_scaler.inverse_transform(y_preds_scaled).reshape(-1, 1)
        
        new_y = np.concatenate((y[:-future_quarters], y_preds))

        preds = handle_format_predictions(pd.DataFrame(new_y), index=df.index)

        return preds

