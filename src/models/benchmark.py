import copy
import os
import numpy as np
import pandas as pd
import importlib

from src.logger import Logger, Log
from src.models.lastValue import LastValue
from src.models.averageWindow import AverageWindow
from src.models.lstm import Lstm
from src.models.prophet import Prophet
from src.tools.metrics import rmse, mape, proto_forecast_ability
from src.data_processing.data_processing import read_stock_prices


def get_bench_model(model_name: str, model_params: dict):

    # Load "module.submodule.model_name"
    model_object = getattr(importlib.import_module("src.models"), model_name)

    # Instantiate the class (pass arguments to the constructor, if needed)
    params_filtered = dict()
    for key, value in model_params.items():
    
        params_filtered[key] = value[0]

    model = model_object(**params_filtered)

    return model


def bench_rec(model_name: str, model_params: dict, index: int, future_days: int, X: pd.DataFrame, logger: Logger):
    
    param = sorted(model_params.keys())[index]
    start, stop, step = model_params[param]

    for i in np.arange(start, stop, step): 

        # Update the current parameter
        model_params[param] = [i, stop, step]

        # Check if we went through all the params
        if index == len(model_params.keys()) - 1:

            # Get the current model with the right parameters
            model = get_bench_model(model_name, model_params)
            # Get predictions and labels
            Y_pred = model.predict(X[:-future_days], future_days=future_days)
            Y_true = X[-future_days:].rename(columns={"yhat": "y"})

            # Get the metric scores
            metric_1 = rmse(Y_true, Y_pred)
            metric_2 = mape(Y_true, Y_pred)
            metric_3 = proto_forecast_ability(Y_true, Y_pred)
            logger.add(Log.INFO, "{}: {} / rmse: {} - mape: {} - fa_score: {}"
                .format(model_name, model.get_params, metric_1, metric_2, metric_3))

        else:

            # Recursive operation
            bench_rec(model_name, model_params.copy(), index + 1, future_days, X, logger)


def benchmark(nb_stocks: int, logger: Logger, models_params: dict):

    future_days = 90
    stock_names = os.listdir("stock_prices")
    indices = np.random.choice(len(stock_names), nb_stocks, replace=False)

    models_params_copy = copy.deepcopy(models_params)

    for i in indices:

        
        df = read_stock_prices(stock_names[i].replace(".csv", ""))

        train_days = np.random.randint(756, 1260)
        df = df.iloc[-train_days:]
        logger.add(Log.INFO, "The stock {} is now used as the benchmark data with {} days".format(stock_names[i], len(df)))

        for model_name, model_params in models_params.items():

            bench_rec(model_name, model_params, 0, future_days, df, logger)

        models_params = copy.deepcopy(models_params_copy)