import pandas as pd

from fbprophet import Prophet as proph
from src.tools.tools import suppress_stdout_stderr, handle_format_predictions
from src.models.model import Model


class Prophet(Model):

    def __init__(self, seasonality=0, changepoint_prior_scale=0.05, fourier_order=0, mcmc_samples=0, uncertainty_samples=1000):

        super(Prophet, self).__init__(
            seasonality=seasonality,
            changepoint_prior_scale=changepoint_prior_scale,
            fourier_order=fourier_order,
            mcmc_samples=mcmc_samples,
            uncertainty_samples=uncertainty_samples
        )

        seasonalities = ["d", "w", "y"]

        if seasonalities[seasonality] == "d":
            self.prophet = proph(
                interval_width=0.95,
                daily_seasonality=True,
                changepoint_prior_scale=changepoint_prior_scale,
                mcmc_samples=mcmc_samples,
                uncertainty_samples=uncertainty_samples
            )
        elif seasonalities[seasonality] == "w":
            self.prophet = proph(
                interval_width=0.95,
                weekly_seasonality=True,
                changepoint_prior_scale=changepoint_prior_scale,
                mcmc_samples=mcmc_samples,
                uncertainty_samples=uncertainty_samples
            )
        elif seasonalities[seasonality] == "y":
            self.prophet = proph(
                interval_width=0.95,
                yearly_seasonality=True,
                changepoint_prior_scale=changepoint_prior_scale,
                mcmc_samples=mcmc_samples,
                uncertainty_samples=uncertainty_samples
            )
        else:
            print("The seasonality is unknown")

        if fourier_order != 0:
            self.prophet.add_seasonality(name='monthly', period=21, fourier_order=fourier_order)


    def predict(self, X: pd.DataFrame, future_days=10):

        # Renaming the columns for use in FB prophet
        X = X.reset_index()
        X = X.rename({"Date": "ds", "yhat": "y"}, axis="columns")

        with suppress_stdout_stderr():
            self.prophet.fit(X)

        future = self.prophet.make_future_dataframe(int(future_days * 10/7))

        future["day"] = future['ds'].dt.weekday
        future = future[future['day']<=4]
        
        predictions = self.prophet.predict(future)

        predictions = predictions.loc[:, ["yhat", "ds"]]

        predictions = handle_format_predictions(predictions)

        return predictions