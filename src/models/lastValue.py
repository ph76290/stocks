import pandas as pd

from src.models.model import Model


class LastValue(Model):

    def __init__(self, N=1):

        super(LastValue, self).__init__(N=N)


    def predict(self, X: pd.DataFrame, future_days=0):

        if self.N >= len(X):

            print("Not enough data to predict with this value of N: {}".format(self.N))
            return []

        X = X.rename(columns={"y": "yhat"})

        predictions = X.shift(self.N).dropna().rename(columns={0: "yhat"})
        
        for _ in range(future_days):
            future_date = predictions[-1:].index
            future_date = future_date.shift(1, freq='D')

            while not future_date.to_pydatetime()[0].isoweekday() in range(1, 6):
                future_date = future_date.shift(1, freq='D')

            val = predictions.iloc[:, 0].values[-self.N]
            predictions = predictions.append(pd.DataFrame({"yhat": val}, index=future_date))
        
        return predictions