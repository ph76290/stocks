import pandas as pd


class Model:

    def __init__(self, **kwargs):

        for key, value in kwargs.items():
            setattr(self, key, value)

        self.get_params = vars(self).copy()


    def predict(self, df: pd.DataFrame, future_days: int):

        pass