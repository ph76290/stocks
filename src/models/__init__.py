from src.models.averageWindow import *
from src.models.lastValue import *
from src.models.lstm import *
from src.models.prophet import *
from src.models.svm import *