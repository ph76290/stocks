import numpy as np
import pandas as pd

import tensorflow as tf
import tensorflow.python.util.deprecation
from sklearn.preprocessing import MinMaxScaler
from tensorflow.python.util import deprecation
deprecation._PRINT_DEPRECATION_WARNINGS = False
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras import optimizers
from tensorflow.keras.layers import Dense, LSTM, Dropout, Input, concatenate
from tensorflow.keras.models import Model
from src.tools.tools import moving_average
from src.models.model import Model as _Model
from src.tools.tools import get_date_range, handle_format_predictions


class Lstm(_Model):

    def __init__(self, sliding_window=60, moving_average_window=0, lstm_type=0, epochs=60):

        super(Lstm, self).__init__(sliding_window=sliding_window, moving_average_window=moving_average_window, lstm_type=lstm_type, epochs=epochs)


    def predict(self, df: pd.DataFrame, future_days=1):

        # Scaling
        min_max_scaler = MinMaxScaler()
        df = pd.DataFrame(min_max_scaler.fit_transform(df)).set_index(df.index)
        data = df.iloc[:, 0].values

        # Get the start bound beginning of the series without the elements to remove
        start_bound = max(self.sliding_window, self.moving_average_window)
        # Filling the new windows data, removing the first 'sw' data
        X = np.zeros((len(data) - start_bound, self.sliding_window))
        y = np.zeros((len(data) - start_bound, future_days))

        for i in range(start_bound, len(data) - future_days + 1):

            X[i - self.sliding_window] = data[i - self.sliding_window:i]
            y[i - self.sliding_window] = data[i:i + future_days]

        X_test = data[-self.sliding_window:]

        X = np.expand_dims(X, axis=-1)
        X_test = np.expand_dims(X_test, axis=-1)
        X_test = np.expand_dims(X_test, axis=0)

        inputs = [X]
        inputs_test = [X_test]

        if self.moving_average_window:
        
            # Expand dims to match the model input
            X_sma = moving_average(data, self.moving_average_window)

            #Handle the size of the serie
            if self.sliding_window > self.moving_average_window:
                X_sma = X_sma[self.sliding_window - self.moving_average_window:]
            X_sma = np.expand_dims(X_sma, axis=-1)
            
            X_test_sma = X_sma[-1]
            X_test_sma = np.expand_dims(X_test_sma, axis=0)

            X_sma = X_sma[:-1]

            # PROBLEM => moving avg window > sliding window

            inputs.append(X_sma)
            inputs_test.append(X_test_sma)


        self.get_model(future_days)
        _ = self.model.fit(x=inputs, y=y, epochs=self.epochs, validation_split=0.2, batch_size=32, shuffle=True, callbacks=[self.early_stopping], verbose=0)

        predictions = self.model.predict(inputs_test)
        
        # Get the future days range
        index = get_date_range(df.index[-1:], future_days)

        df_preds = handle_format_predictions(pd.DataFrame(predictions[0]), index=index)

        # Concatenate the train data and the preds

        predictions = df.rename(columns={0: "yhat"}).append(df_preds)

        index = predictions.index

        # Inverse transform the predictions
        predictions = pd.DataFrame(min_max_scaler.inverse_transform(predictions))
        # Get good format for predictions
        predictions = handle_format_predictions(predictions, index=index)

        return predictions

    
    def get_model(self, output_nb: int):

        adam = optimizers.Adam(lr=0.0005)

        self.early_stopping = EarlyStopping(
            monitor='val_loss',
            min_delta=0,
            patience=5,
            verbose=0,
            mode='auto'
        )

        input_lstm = Input(shape=(self.sliding_window, 1))

        lstm_1 = LSTM(50)(input_lstm)
        dropout_1 = Dropout(0.2)(lstm_1)

        if self.moving_average_window:
            
            input_dense = Input(shape=(1,))
            
            model_1 = Model(inputs=input_lstm, outputs=dropout_1)

            dense_2 = Dense(8, activation='relu')(input_dense)
            dropout_2 = Dropout(0.2)(dense_2)

            model_2 = Model(inputs=input_dense, outputs=dropout_2)

            concat_1 = concatenate([model_1.output, model_2.output])
            dense_3 = Dense(16, activation="sigmoid")(concat_1)
            
            output = Dense(output_nb, activation='linear')(dense_3)

            self.model = Model(inputs=[model_1.input, model_2.input], outputs=output)

        else:

            dense_4 = Dense(32, activation="sigmoid")(dropout_1)

            output = Dense(output_nb, activation="linear")(dense_4)

            self.model = Model(inputs=input_lstm, outputs=output)

        self.model.compile(loss="mean_squared_error", optimizer=adam)