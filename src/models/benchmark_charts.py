import pandas as pd
import matplotlib
from matplotlib import pyplot as plt


def get_charts(benchmark_file: str):

    bench_results = dict()
    best_params_all = dict()

    with open(benchmark_file, "r") as f:

        l = f.readlines()
        l = list(filter(lambda x: '{' in x and '}' in x and ':' in x, map(lambda x: x.split("]: ")[1].replace("\n", ""), l)))

        for i in range(len(l)):

            split =  l[i].split(": {")
            model_name = split[0]

            if not model_name in bench_results.keys():

                bench_results[model_name] = []

            split_2 = split[1].split("} /")

            params = list(map(lambda x: x.replace("'", "").replace(" ", ""), split_2[0].split(",")))
            metrics = list(map(lambda x: x.replace(" ", ""), split_2[1].split(" - ")))

            params_dico = dict()

            for param in params:
                
                split_3 = param.split(":")
                param_key = split_3[0]
                param_value = split_3[1]

                params_dico[param_key] = param_value

            metrics_dico = dict()

            for metric in metrics:
                
                split_3 = metric.split(":")
                metric_key = split_3[0]
                metric_value = split_3[1]

                metrics_dico[metric_key] = metric_value

            dico = {"metrics": metrics_dico, "params": params_dico}
            bench_results[model_name].append(dico)


        for model_name, model_rows in bench_results.items():

            dico = dict()

            for row in model_rows:

                for param_key, param_value in row["params"].items():

                    if not param_key in dico.keys():

                        dico[param_key] = {"value": [param_value]}

                        for metric_key, metric_value in row["metrics"].items():

                            dico[param_key][metric_key] = [metric_value]

                    else:

                        dico[param_key]["value"].append(param_value)

                        for metric_key, metric_value in row["metrics"].items():

                            dico[param_key][metric_key].append(metric_value)


            plt.figure(figsize=(30, 20))
            matplotlib.rcParams.update({'font.size': 22})
            colors = ["r", "g", "b", "y", "m", "c", "k"]

            best_params = dict()

            for i, key in enumerate(dico.keys()):

                df = pd.DataFrame.from_dict(dico[key], orient='index', dtype="float32").transpose()
                df = df.groupby(["value"], axis=0).mean()

                for j, key_metric in enumerate(df.columns):
                        
                    plt.subplot(len(dico), len(dico[key]) - 1, i * (len(dico[key]) - 1) + j + 1)
                    plt.title("Model: {}".format(model_name))
                    plt.xlabel(key)
                    plt.ylabel(key_metric)
                    plt.plot(df.index, df[key_metric], c=colors[j % len(colors)])

                best_index = 0

                if len(df) > 1:

                    min_max_scaled = (df - df.min()) / (df.max() - df.min())
                    aggregate_metrics = min_max_scaled.sum(axis=1) / len(df.columns)
                    best_index = aggregate_metrics.argmin()

                best_value = df.index[best_index]
                if key in ["changepoint_prior_scale"]:
                    best_params[key] = float(best_value)
                else:
                    best_params[key] = int(best_value)

            plt.show()
        
            best_params_all[model_name] = best_params

    return best_params_all