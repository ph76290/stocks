import sys
import os
import numpy as np

from enum import Enum
from datetime import datetime
from time import gmtime, strftime


class Log(Enum):
    ERROR = 0
    INFO = 1
    DEBUG = 2

    def __int__(self):
        return self.value


# Class used to store logs in a file stream
class Logger():

    def __init__(self, filename: str, verbose: int):


        # Check if the filename is not empty
        if filename == "":

            # Use a filename with the date as a name
            filename = datetime.now().strftime("log_%d-%m-%Y_%H-%M-%S.txt")

        # Set the destination in the folder logs
        if not filename.startswith("logs"):
            filename = os.path.join("logs", filename)

        # Open the file to work with file director
        self.file = sys.stdout if filename == "logs/stdout" else open(filename, "w+")
        
        # Associate the logger with a bot
        self.verbose = verbose
        self.filename = filename

        self.add(Log.INFO, "A new logger has been instantiated !")
        self.add(Log.INFO, "The logger will display logs in {}"
            .format(self.filename))


    def add(self, type: Log, message: str):

        # Add a message from a bot name to the stream with a certain type (INFO, ERROR)
        if int(type) <= self.verbose:
            self.file.write("[LOG][{}][{}]: {}\n".format(strftime("%Y-%m-%d %H:%M:%S", gmtime()), type.name.upper(), message))

    
    def add_return_line(self, type: Log, message: str):

        # Add a message at the line
        if int(type) <= self.verbose:
            self.file.write(message + "\n")


    def add_model_history(self, history):

        # Pretty print the model history
        self.add(Log.INFO, "Printing the model's metric values over the epochs")

        if self.verbose:
            
            nb_epochs = len(history["loss"])
            keys = history.keys()

            # Iterate over the epochs
            for epoch in range(nb_epochs):

                # Build each line with the appropriate values
                message = "\t\tEpoch {}/{}   =>   ".format(epoch + 1, nb_epochs)
                for i, metric in enumerate(keys):

                    message += metric + ": {:05f}".format(round(history[metric][epoch], 5))

                    if i != len(keys) - 1:
                        message += " - "

                # Add the info line
                self.add_return_line(Log.INFO, message)


    def add_binary_confusion_matrix(self, matrix: np.array):

        # Pretty print the confusion matrix
        self.add(Log.INFO, "Printing the confusion matrix for the given predictions")

        if self.verbose:

            self.add_return_line(Log.INFO, "\t\t\t    Predictions")
            self.add_return_line(Log.INFO, "\t\t    |    0\t|\t1")
            self.add_return_line(Log.INFO, "\t\t    |-----------|------------")
            self.add_return_line(Log.INFO, "\t\t0   |  {}\t|\t  {}".format(matrix[0][0], matrix[0][1]))
            self.add_return_line(Log.INFO, "\tLabels      |-----------|------------")
            self.add_return_line(Log.INFO, "\t\t1   |  {}\t|\t  {}".format(matrix[1][0], matrix[1][1]))


    def kill(self):

        # This method close the stream
        self.add(Log.INFO, "Last line of the logs, killing the logger...")

        # Close the fd only if it is not stdout
        if self.file is not sys.stdout:
            self.file.close()
