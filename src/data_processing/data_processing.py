import pandas as pd
import pickle
import random
import numpy as np

from sklearn.decomposition import PCA



def read_data(stock_name: str, train_days=3000, test_days=0, threshold_correlation=0.5, pca_components=0):

    with open('stock_data/stock_info/{}.pkl'.format(stock_name), 'rb') as fd:

        stock_data = pickle.load(fd)
        df_total_data = pd.DataFrame.from_dict(stock_data["data"]).sort_index()
        df_total_data = df_total_data.dropna(axis=1, how='all')
        df_total_data = df_total_data.fillna(method='ffill')
        df_total_data = df_total_data.replace(to_replace='None', value=np.nan).dropna()

        print(df_total_data.columns)
        print(df_total_data.loc[df_total_data["volume"] != np.nan, ["volume", "Volume"]])
        print(df_total_data.volume.unique())

        return df_total_data



def read_stock_prices(stock_name: str, train_days=3000, test_days=0):

    # Reading data
    df = pd.read_csv("stock_prices/{}.csv".format(stock_name), parse_dates=True, index_col="Date")
    df = df.loc[:, ["Close"]].rename(columns={"Close": "yhat"})

    # Splitting
    
    if test_days:
        df_train = df.iloc[-train_days - test_days:-test_days]
        df_test = df.iloc[-test_days:]
        df_test = df_test.rename(columns={"yhat": "y"})
        
        return df_train, df_test

    else:
        df_train = df.iloc[-train_days:]
        return df_train


def read_fundamental_data(data_file: str, threshold_correlation=0.5, pca_components=0):

    nb_fundamental_stocks = 750

    corr_df = dict()
    correct_fundamental_stocks = dict()

    with open(data_file, 'rb') as pickle_file:
        
        content = pickle.load(pickle_file)
        fundamental_stocks = dict(random.sample(content.items(), nb_fundamental_stocks))
        
        for stock_name, stock_df in fundamental_stocks.items():

            stock_df = stock_df.replace(to_replace='None', value=np.nan).dropna()
            stock_df = stock_df.apply(pd.to_numeric, errors="ignore")
            uniques_columns = stock_df.nunique().index[stock_df.nunique() <= 2].tolist()
            stock_df = stock_df.drop(columns=uniques_columns)

            if len(stock_df) <= 2 or "New Price" in uniques_columns:
                continue

            # Reverse the order of the df to be the chronological order
            stock_df = stock_df.iloc[::-1]

            stock_df["New Price"] = stock_df["Price"].shift(-1)
            stock_df = stock_df.set_index("Quarter end")
            stock_df.index = pd.to_datetime(stock_df.index).shift(-91, freq="D")

            if threshold_correlation:

                stock_df_but_last = stock_df.iloc[:-1]

                for column in stock_df_but_last.columns:

                    corr = stock_df_but_last[column].corr(stock_df_but_last["New Price"])

                    if column in corr_df.keys():
                        corr_df[column].append(corr)

                    else:
                        corr_df[column] = [corr]

            correct_fundamental_stocks[stock_name] = stock_df


    if threshold_correlation:
       
        useless_columns = []

        for column_name, corr in corr_df.items():

            l = len(corr)
            c = list(map(lambda x: abs(x), corr))
            s = sum(c)

            res = s / l

            if sum(corr) < 0:
                res *= -1

            print("{}: {}".format(column_name, round(res, 5)))

            if abs(res) < threshold_correlation:

                useless_columns.append(column_name)
       
        for stock_name, df in correct_fundamental_stocks.items():

            drop_columns = [col for col in useless_columns if col in df.columns]
            correct_fundamental_stocks[stock_name] = df.drop(columns=drop_columns)
        
    elif pca_components:

        for stock_name, stock_df in correct_fundamental_stocks.items():
        
            pca = PCA(n_components=min(pca_components, len(stock_df)))
            index = stock_df.index
            columns = ["artificial_" + str(i) for i in range(min(pca_components, len(stock_df)))]
            price_column = stock_df["New Price"]
            stock_df = stock_df.drop(columns=["New Price"])
            new_df = pd.DataFrame(pca.fit_transform(stock_df), index=index, columns=columns).merge(price_column, left_index=True, right_index=True)
            correct_fundamental_stocks[stock_name] = new_df

    return correct_fundamental_stocks