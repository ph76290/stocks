import datetime
import os
import numpy as np
import pandas as pd
import pickle
import yahooquery
import FundamentalAnalysis as fa
from src.scraping.yahoofinancialsgit.yahoofinancials import YahooFinancials

# stocksight token e0a01778ccb574d55b1191b16876bdeb060e4a553dc284794c00de37fd0404a9d85cd0508f158fb31f201f8fbae5a79fa7fc

def read_total_data(filename: str):

    df_total_data = pd.DataFrame()
    if os.path.isfile(filename):

        with open(filename, "rb") as fd:
            stock_data = pickle.load(fd)
            df_total_data = pd.DataFrame.from_dict(stock_data["data"]).sort_index()

    return df_total_data


def get_yahoo_data(stock_name: str, df_total_data: pd.DataFrame, stock_data: dict):

    df_recent_data = pd.DataFrame()
    yahoo_fin = YahooFinancials(stock_name.lower())
    end_date_index = "1980-01-01" if df_total_data.empty else df_total_data.index[-1]
    today = datetime.date.today().strftime("%Y-%m-%d")
    historical_prices = yahoo_fin.get_historical_price_data(end_date_index, today, "daily")
    # Get prices, dividends and key infos

    stock_name = stock_name.upper()
    if historical_prices[stock_name]['eventsData'] != {}:

        infos = ['firstTradeDate', 'currency', 'instrumentType', 'timeZone']

        for key in infos:
            if key in historical_prices[stock_name]:
                stock_data[key] = historical_prices[stock_name][key]

        df_recent_data = pd.DataFrame.from_dict(historical_prices[stock_name]['prices']).set_index("formatted_date").drop(columns=["date"])

    # Get quarterly income, cashflow and balance information

    try:

        income = fa.income_statement(stock_name, period="quarter").transpose()
        balance_sheet = fa.balance_sheet_statement(stock_name, period="quarter").transpose()
        cashflow = fa.cash_flow_statement(stock_name, period="quarter").transpose()
        key_metrics = fa.key_metrics(stock_name, period="quarter").transpose()
        for i, statmnt in enumerate([income, balance_sheet, cashflow, key_metrics]):

            df_stock_income = statmnt
            df_stock_income.index = list(map(lambda x: x + "-31" if int(x.split('-')[1]) % 9 == 3 else x + "-30", df_stock_income.index))

            df_recent_data = pd.merge(df_recent_data, df_stock_income, right_index=True, left_index=True, how='outer')
        
    except KeyError:
        print("A Key Error exception was raised for the stock {}".format(stock_name))

    df_recent_data = df_recent_data.sort_index()

    return df_recent_data, stock_data


def get_additional_data(stock_name: str, df_recent_data: pd.DataFrame, stock_data: dict):
    
    # Declare filenames
    prices_file = "stock_data/stock_prices/{}.csv".format(stock_name)
    prices_simfin_file = "stock_data/stock_prices/us-shareprices-daily.csv"
    companies_simfin_file = "stock_data/stock_fundamental/us-companies.csv"
    industries_simfin_file = "stock_data/stock_fundamental/industries.csv"
    income_simfin_file = "stock_data/stock_fundamental/us-income-quarterly.csv"
    balance_simfin_file = "stock_data/stock_fundamental/us-balance-quarterly.csv"
    cashflow_simfin_file = "stock_data/stock_fundam=ental/us-cashflow-quarterly.csv"

    if os.path.isfile(companies_simfin_file):

        df_companies = pd.read_csv(companies_simfin_file, sep=';')

        if stock_name in df_companies.Ticker:
        
            stock_simfin_id = df_companies.loc[df_companies["Ticker"] == stock_name].SimFinId.iloc[0]
            stock_industry_id = df_companies.loc[df_companies["Ticker"] == stock_name].IndustryId.iloc[0]

            if os.path.isfile(industries_simfin_file):
                df_industries = pd.read_csv(industries_simfin_file, sep=';')
                industry_row = df_industries.loc[df_industries["IndustryId"] == stock_industry_id]
                stock_data["sector"] = industry_row.Sector.values[0]
                stock_data["industry"] = industry_row.Industry.values[0]

    if os.path.isfile(prices_file):

        df_prices = pd.read_csv(prices_file).set_index("Date").rename(columns={"Open": "open", "High": "high", "Close": "close", "Low": "low"})
        print(df_prices.columns)
        start_date = df_recent_data.index[0] if not df_recent_data.empty else (datetime.date.today() + datetime.timedelta(1)).strftime("%Y_%m-%d")
        if df_prices.first_valid_index() < start_date:

            df_prices = df_prices.loc[df_prices.index < start_date]
            df_recent_data = df_prices.append(df_recent_data)

        
    if os.path.isfile(prices_simfin_file) and stock_name in df_companies.Ticker:

        df_prices = pd.read_csv(prices_simfin_file, sep=';').set_index("Date").rename(columns={"Open": "open", "High": "high", "Close": "close", "Low": "low"})
        df_prices = df_prices.loc[df_prices["SimFinId"] == stock_simfin_id]

        start_date = df_recent_data.index[0] if not df_recent_data.empty else (datetime.date.today() + datetime.timedelta(1)).strftime("%Y_%m-%d")

        if df_prices.index[0] < start_date:

            df_prices = df_prices.loc[df_prices.index < start_date]
            df_recent_data = df_prices.append(df_recent_data)

    """
    if os.path.isfile(income_simfin_file):

        df_income = pd.read_csv(income_simfin_file, sep=';').set_index('Report Date')
        df_stock_income = df_income.loc[df_income["SimFinId"] == stock_simfin_id]
        df_stock_income = df_stock_income.drop(columns=['Ticker', 'SimFinId', 'Currency', 'Fiscal Year', 'Publish Date'])
        df_stock_income['Cost of Revenue'] = df_stock_income['Cost of Revenue'].apply(lambda x: -x)
        pd.set_option("display.max_rows", None, "display.max_columns", None)

        print(df_recent_data.loc['2019-03-31'])
        print(df_stock_income.loc["2019-03-31"])
        df_stock_income = df_stock_income.rename(columns={
            'Shares (Basic)': 'BasicAverageShares',
            'Shares (Diluted)': 'DilutedAverageShares',
            'Revenue': 'TotalRevenue',
            'Cost of Revenue': 'CostOfRevenue',
            ''
            'Gross Profit': 'grossProfit',
            'Research & Development': 'researchDevelopment',
            'Selling, General & Administrative': 'sellingGeneralAdministrative', # Doesn't match
            # 'nonRecurring'
            # 'otherOperatingExpenses'
            # 'totalOtherIncomeExpenseNet'
            'Interest Expense, Net': 'interestExpense',
            'Pretax Income (Loss)': 'incomeBeforeTax',
            'Income (Loss) from Continuing Operations': 'netIncomeFromContinuingOps',
            'Net Income': 'netIncome_x',
            'Net Income (Common)': 'netIncomeApplicableToCommonShares',
            'Income Tax (Expense) Benefit, Net': 'incomeTaxExpense', # How to deal with + and -
            'Depreciation & Amortization': 'depreciation', # Full nans
            'Net Extraordinary Gains (Losses)': 'extraordinaryItems', # Full nans
            'Operating Expenses': 'totalOperatingExpenses', # Doesn't match
            'Operating Income (Loss)': 'operatingIncome', # Columns doesn't match
            #'Non-Operating Income (Loss)': "totalOperatingExpenses"# Columns doesn't match
            # 'Abnormal Gains (Losses)',
        })

        different_columns = list(set(df_stock_income.columns).union(set(df_recent_data.columns)) - set(df_stock_income.columns).intersection(set(df_recent_data.columns)))

        df_recent_data = df_stock_income.append(df_recent_data)
        df_recent_data.index = df_recent_data.index.rename('Date')

        df_recent_data = df_recent_data.drop(columns=["researchDevelopment"])

    #    , 'discontinuedOperations',
    #     'effectOfAccountingCharges',
    #     'minorityInterest_x', 'ebit',
    #    , , 
    #    , 'capitalSurplus', ,
    #    , 'minorityInterest_y', 'otherCurrentLiab',
    #    , 'commonStock', 'otherCurrentAssets', ,
    #    'otherLiab', 'goodWill' , 'cash',
    #    , 'otherStockholderEquity' , 'netTangibleAssets',
    #     'netBorrowings',
    #    'issuanceOfStock', ,
    #    'effectOfExchangeRate', ,
    #    'changeToNetincome', 


    if os.path.isfile(balance_simfin_file):

        df_balance = pd.read_csv(balance_simfin_file, sep=';').set_index('Report Date')
        df_stock_balance = df_balance.loc[df_balance["SimFinId"] == stock_simfin_id]
        df_stock_balance = df_stock_balance.drop(columns=['Ticker', 'SimFinId', 'Currency', 'Fiscal Year', 'Publish Date'])
        df_stock_balance = df_stock_balance.rename(columns={
            'Total Assets': 'totalAssets',
            'Total Current Assets': 'totalCurrentAssets',
            'Total Current Liabilities': 'totalCurrentLiabilities',
            'Total Liabilities': 'totalLiab',
            'Inventories': 'inventory',
            'Retained Earnings': 'retainedEarnings',
            'Payables & Accruals': 'accountsPayable',
            'Cash, Cash Equivalents & Short Term Investments': 'totalCashFromOperatingActivities',#'investments' # Doesn't match
            'Accounts & Notes Receivable': 'changeToAccountReceivables', # Doesn't match
            'Long Term Investments & Receivables': 'longTermInvestments', # Doesn't match
            'Total Noncurrent Assets': 'otherAssets', # Doesn't match
            'Long Term Debt': 'longTermDebt', # Doesn't match (almost 1.5 x 10⁷)
            'Treasury Stock': 'treasuryStock', # Doesn't match
            'Total Equity': 'totalStockholderEquity', # Doesn't match
            'Short Term Debt': 'shortLongTermDebt', # Doesn't match
            'Other Long Term Assets': 'deferredLongTermAssetCharges', # Doesnt match
            'Property, Plant & Equipment, Net': 'propertyPlantEquipment', # Doesn't match
            # 'Fiscal Period',
            # 'Total Noncurrent Liabilities', #'changeToLiabilities'
            # 'Share Capital & Additional Paid-In Capital',
            # 'Total Liabilities & Equity'
        })
    if os.path.isfile(cashflow_simfin_file):

        df_cashflow = pd.read_csv(cashflow_simfin_file, sep=';').set_index('Report Date')
        df_stock_cashflow = df_cashflow.loc[df_cashflow["SimFinId"] == stock_simfin_id]
        df_stock_cashflow = df_stock_cashflow.drop(columns=['Ticker', 'SimFinId', 'Currency', 'Fiscal Year', 'Publish Date'])
        df_stock_cashflow = df_stock_cashflow.rename(columns={
            'Dividends Paid': 'dividendsPaid',
            'Net Cash from Financing Activities': 'totalCashFromFinancingActivities',
            'Net Change in Cash': 'changeInCash',
            'Net Cash from Investing Activities': 'totalCashflowsFromInvestingActivities',
            'Net Cash from Operating Activities': 'changeToOperatingActivities', # Doesn't match (almost)
            'Change in Inventories': 'changeToInventory', # First one full of nan 
            'Change in Fixed Assets & Intangibles': 'intangibleAssets', # Doesn't match
            'Change in Accounts Receivable': 'netReceivables',# First one full of nan
            'Non-Cash Items': 'otherItems', # other items full of nans
            'Cash from (Repurchase of) Equity': 'repurchaseOfStock', # Doesn't match (almost 7x10⁶)
            'Change in Other': 'otherCashflowsFromFinancingActivities', # First one full of nans
            'Net Change in Long Term Investment': 'otherCashflowsFromInvestingActivities', # Doesn't match
            # 'Net Cash from Acquisitions & Divestitures',
            # 'Change in Accounts Payable',
            # 'Cash from (Repayment of) Debt',
            # 'Change in Working Capital',#'capitalExpenditures'
        })
    """
    return df_recent_data, stock_data