import datetime
import numpy as np
import os
import pandas as pd
import pandas_market_calendars as mcal
import pickle

from src.scraping.tools import get_yahoo_data, read_total_data, get_additional_data

    
def update_stock_information(stock_name: str):

    stock_data = dict()
    pickle_file = "stock_data/stock_info/{}.pkl".format(stock_name)
   
    # Get information that we already have stored from the current company
    df_total_data = read_total_data(pickle_file)
    
    # Get the recent data from yahoo
    df_recent_data, stock_data = get_yahoo_data(stock_name, df_total_data, stock_data)

    # Get additional data from simfin and from the csvs already stored
    df_recent_data, stock_data = get_additional_data(stock_name, df_recent_data, stock_data)

    # Trick to merge rows with ssame indices
    """
    for column in different_columns:
        df_recent_data.groupby(['Date'])[column].apply(lambda x: ','.join(x.astype(str)))
        df_recent_data[column] = df_recent_data[column].apply(lambda x: str(x).replace("nan", "").replace(",", ""))
        df_recent_data = df_recent_data.replace('', np.nan)
    """
    # Format dataframe and process it to make sure the data is correct before saving
    df_total_data = df_total_data.append(df_recent_data)
    df_total_data = df_total_data.drop_duplicates().sort_index()
    df_total_data = df_total_data.fillna(method='ffill')
    stock_data["data"] = df_total_data.to_dict()
    
    # Write the new data retrieved
    with open('stock_data/stock_info/{}.pkl'.format(stock_name), 'wb') as f:
        pickle.dump(stock_data, f)



def check_data(stock_name: str):

    # Check index
    # check start date and end date
    # check that non trading days are not part of the df
    # check if there are missing days in the date range of the index

    # Create a calendar
    nyse = mcal.get_calendar('NYSE')

    # Show available calendars
    pickle_file = "stock_data/stock_info/{}.pkl".format(stock_name)


    with open(pickle_file, "rb") as fd:
        stock_data = pickle.load(fd)
        df_total_data = pd.DataFrame.from_dict(stock_data["data"]).sort_index()
    df_total_data = df_total_data.dropna(axis=1, how='all')
    df_total_data = df_total_data.fillna(method='ffill')
    pd.set_option('display.max_rows', 200 + 1)
    print(df_total_data.tail(200))
    print(df_total_data.info())
    start_date, end_date = df_total_data.index[0], df_total_data.index[-1]

    print("The start date of the information on {} is {} and the end date is {}".format(stock_name, start_date, end_date))
    early = nyse.schedule(start_date=start_date, end_date=end_date)
    index_to_compare = early.index.strftime("%Y-%m-%d")
    
    booleans = df_total_data.index.isin(index_to_compare)
    indices = [not i for i in booleans]
    not_trading_days = df_total_data.index[indices]

    booleans2 = index_to_compare.isin(df_total_data.index)
    indices2 = [not i for i in booleans2]
    missing_trading_days = index_to_compare[indices2]

    print("The days that are not trading days are {}".format(not_trading_days))
    print("The trading days days that are missing in the date range are {}".format(missing_trading_days))

    # check data
    # check if every 3 months we have the quarterly reports data instead of nan
    # check if the data columns are coherent after assembling the df from many different sources of data