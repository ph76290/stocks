import _pickle as pickle
import glob
import os
import pandas as pd
import time

#from selenium import webdriver
#from src.scraping.tools import get_driver_ready, get_stock_page
# from alpha_vantage.timeseries import TimeSeries
# import matplotlib.pyplot as plt

# ts = TimeSeries(key='75Z3U39593FWXI8F', output_format='pandas')
# data, meta_data = ts.get_intraday(symbol='MSFT',interval='1min', outputsize='full')
# print(data)
# data['4. close'].plot()
# plt.title('Intraday Times Series for the MSFT stock (1 min)')
# plt.show()


# API KEY ALPHA VANTAGE 75Z3U39593FWXI8F
# API KEY QUANDL 7CSoTkzRE2PwwrPxRLBy

def get_stock_information(stock_name: str, background=True):

    pass

def get_stock_prices(driver: webdriver):

    pass

def get_fundamental_data(driver: webdriver):

    pass





def grab_csv():
    """
    Loading in the csv file with closing prices downloaded from Yahoo finance
    """
    # File list retrieved from local download folder
    list_of_files = glob.glob('~/Documents/stocks/stock_prices/*.csv') 
    
    # Assigning the most recent file
    latest_file = max(list_of_files, key=os.path.getctime)
    return pd.read_csv(latest_file)

# Checking the dataframe
df = grab_csv()

with open("stock_df.pkl", "wb") as fp:
    pickle.dump(df, fp)