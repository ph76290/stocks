import math
import pandas as pd


def rmse(Y_true: pd.DataFrame, Y_pred: pd.DataFrame):

    df = pd.merge(Y_true, Y_pred, right_index=True, left_index=True)

    df = df.rename(columns={0: "y"})

    values = df.y - df.yhat
    se = values * values
    mse = se.sum() / len(se)
    rmse = math.sqrt(mse)

    return round(rmse, 4)


def mape(Y_true: pd.DataFrame, Y_pred: pd.DataFrame):

    df = pd.merge(Y_true, Y_pred, right_index=True, left_index=True)

    df = df.rename(columns={0: "y"})

    values = df.y - df.yhat
    mape = abs(values / df.y)
    mape = mape.sum() / len(mape)

    return round(mape, 4)


def proto_forecast_ability(Y_true: pd.DataFrame, Y_pred: pd.DataFrame, gamma=0.95):

    df = pd.merge(Y_true, Y_pred, right_index=True, left_index=True)

    nb_days_predicted = len(df)

    batch_percent = 10
    days_to_decrease_percent = max(int(batch_percent * nb_days_predicted / 100), 1)

    fa_score = 0

    for i in range(nb_days_predicted, 0, -days_to_decrease_percent):

        start = max(0, i - days_to_decrease_percent)
        values = df.iloc[start:i, :]

        slope_true = values.y[0] / values.y[-1]
        slope_pred = values.yhat[0] / values.yhat[-1]
        slope_diff = abs(slope_pred - slope_true) * 100

        values = values.y - values.yhat
        se = values * values
        mse = se.sum() / len(se)
        rmse = math.sqrt(mse)

        current_fa_score = rmse * slope_diff * gamma

        fa_score += current_fa_score

        gamma *= 0.95

    return round(fa_score, 4)