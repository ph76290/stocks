import pandas as pd

import matplotlib.patches as mpatches
from matplotlib import pyplot as plt

from src.tools.metrics import rmse, proto_forecast_ability


def visualize(data: pd.DataFrame, df_test:pd.DataFrame, predictions: dict):

    plt.figure(figsize=(40, 25))

    patches = []
    colors = ["b", "m", "k", "r", "c"]
    markers = ["o", "^", "+", ".", ","]
    dashes = ["-", "--", "-.", ":"]
    plt.plot(data, 'y')
    plt.plot(df_test, 'g')
    patches.append(mpatches.Patch(color='y', label="Train data"))
    patches.append(mpatches.Patch(color='g', label="Test data"))

    for index, (name, pred) in enumerate(predictions.items()):

        label = "{} / rmse: {} - FA score: {}".format(name, rmse(df_test, pred), proto_forecast_ability(df_test, pred))
        patches.append(mpatches.Patch(color=colors[index], label=label))
        display = colors[index] + markers[index % len(markers)] + dashes[index % len(dashes)]
        plt.plot(pred, display, ms=2, alpha=0.25)

    plt.legend(handles=patches, handlelength=4)
    leg = plt.legend(handles=patches, loc='upper left', labelspacing=1.5, handlelength=4, prop={'size': 24})

    for patch in leg.get_patches():
        patch.set_height(22)
        patch.set_y(-6)

    plt.title("Daily")
    plt.ylabel('Stock Price')
    plt.xlabel('Date')

    plt.show()


def visualize_test(df_test:pd.DataFrame, predictions: dict):

    plt.figure(figsize=(30, 18))

    patches = []
    colors = ["b", "m", "k", "r", "c"]
    markers = ["o", "^", "+", ".", ","]
    dashes = ["-", "--", "-.", ":"]
    plt.plot(df_test, 'g')
    patches.append(mpatches.Patch(color='g', label="Test data"))

    start_date = df_test.index[0]

    for index, (name, pred) in enumerate(predictions.items()):

        pred = pred.loc[start_date:, :]

        label = "{} / rmse: {} - FA score: {}".format(name, rmse(df_test, pred), proto_forecast_ability(df_test, pred))
        patches.append(mpatches.Patch(color=colors[index], label=label))
        display = colors[index] + markers[index % len(markers)] + dashes[index % len(dashes)]
        plt.plot(pred, display, ms=2, alpha=0.6)

    plt.legend(handles=patches, handlelength=4)
    leg = plt.legend(handles=patches, loc='best', labelspacing=1.5, handlelength=4, prop={'size': 24})

    for patch in leg.get_patches():
        patch.set_height(22)
        patch.set_y(-6)

    plt.title("Daily")
    plt.ylabel('Stock Price')
    plt.xlabel('Date')

    plt.show()