import datetime, pytz, holidays
import os
import numpy as np
import pandas as pd


class suppress_stdout_stderr(object):
    '''
    A context manager for doing a "deep suppression" of stdout and stderr in
    Python, i.e. will suppress all print, even if the print originates in a
    compiled C/Fortran sub-function.
       This will not suppress raised exceptions, since exceptions are printed
    to stderr just before a script exits, and after the context manager has
    exited (at least, I think that is why it lets exceptions through).

    '''
    def __init__(self):
        # Open a pair of null files
        self.null_fds = [os.open(os.devnull, os.O_RDWR) for x in range(2)]
        # Save the actual stdout (1) and stderr (2) file descriptors.
        self.save_fds = (os.dup(1), os.dup(2))

    def __enter__(self):
        # Assign the null pointers to stdout and stderr.
        os.dup2(self.null_fds[0], 1)
        os.dup2(self.null_fds[1], 2)

    def __exit__(self, *_):
        # Re-assign the real stdout/stderr back to (1) and (2)
        os.dup2(self.save_fds[0], 1)
        os.dup2(self.save_fds[1], 2)
        # Close the null files
        os.close(self.null_fds[0])
        os.close(self.null_fds[1])


# Compute the moving average
def moving_average(a, n) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return np.array(ret[n - 1:] / n)


# Check if a day is a business day
def isBusinessDay(day):
    
        tz = pytz.timezone('US/Eastern')
        us_holidays = holidays.US()

        if not day:
            day = datetime.datetime.now(tz)
        # If a holiday
        if day.strftime('%Y-%m-%d') in us_holidays:
            return False
        # If it's a weekend
        if day.date().weekday() > 4:
            return False

        return True

def get_date_range(start_day, future_days: int):

    date_range = start_day

    while len(date_range) < future_days:

        future_date = date_range[-1:]
        future_date = future_date.shift(1, freq='D')

        while not isBusinessDay(future_date[0]):
            future_date = future_date.shift(1, freq='D')
        date_range = date_range.append(future_date)

    return date_range


def handle_format_predictions(predictions: pd.DataFrame, index=None):

    predictions = predictions.rename(columns={0: "yhat"})
    
    if index is None:
        predictions.set_index("ds", inplace=True)

    else:
        predictions.index = index
    
    predictions.index.names = ["Date"]

    return predictions