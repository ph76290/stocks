import pandas as pd
import FundamentalAnalysis as fa 
import csv
import os

# Show the available companies
companies = fa.available_companies()

for index, ticker in enumerate(companies.index):

    for data_type in ["income-statement", "balance-sheet-statement", "cash-flow-statement"]:

        filename = "tmp_full_data/{}_{}.csv".format(ticker, data_type)
        if os.path.isfile(filename):
            continue

        url = "https://financialmodelingprep.com/api/v3/financials/{}/{}?datatype=csv".format(data_type, ticker)
        df = pd.read_csv(url)

        if df.columns[0] == '{':
            continue

        df = df.transpose()
        df.to_csv(filename)

    print("{} / {}".format(index + 1, len(companies)))